# Use an official PHP runtime as a parent image
FROM php:7.3

# Set the working directory to /var/www/html
WORKDIR /var/www/html

# Install required packages
RUN apt-get update && apt-get install -y \
    git \
    libxml2-dev \
    unzip \
    && docker-php-ext-install xml

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Clean up unnecessary packages
RUN apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/*

# Expose port 80 for the web application
EXPOSE 80

# Start PHP built-in server
CMD ["php", "-S", "0.0.0.0:80"]
