<?php

namespace Src\models;

use Src\helpers\Helpers;

class DogModel
{

    private $dogData;

    function __construct()
    {
        $this->helper  = new Helpers();
        $string        = file_get_contents(dirname(__DIR__) . '/../scripts/dogs.json');
        $this->dogData = json_decode($string, true);
    }

    public function getDogs()
    {
        return $this->dogData;
    }

    public function getDogsByClient(int $clientId)
    {
        return array_filter($this->getDogs(), static function ($dog) use ($clientId) {
            return $dog['clientid'] === $clientId;
        });
    }

    public function calculateAgeAverage(array $dogs)
    {
        $ageSum = 0;

        foreach ($dogs as $dog) {
            $ageSum += $dog['age'];
        }

        return $ageSum / count($dogs);
    }

    public function calculateDiscount(int $clientId, $price)
    {
        $clientDogs = $this->getDogsByClient($clientId);

        if (empty($clientDogs)) {
            return 0;
        }

        $ageAverage = $this->calculateAgeAverage($clientDogs);

        if ($ageAverage < 10) {
            return $price * 10 / 100;
        }

        return 0;
    }
}