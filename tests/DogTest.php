<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Booking;
use Src\controllers\Client;
use Src\controllers\Dog;
use Src\models\DogModel;

class DogTest extends TestCase
{

    private $dog;

    /**
     * Setting default data
     *
     * @throws \Exception
     */
    public function setUp()
    : void
    {
        parent::setUp();
        $this->dog = new Dog();

    }

    /** @test */
    public function calculateDiscount()
    {
        $dogModel = new DogModel();

        $discount = $dogModel->calculateDiscount(1, 200);
        $this->assertEquals($discount, 20);

        $discount = $dogModel->calculateDiscount(6, 200);
        $this->assertEquals($discount, 0);
    }
}